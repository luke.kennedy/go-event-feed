Go Event Feed.

Directly copied with no (current) modifications from [go-etherium](https://github.com/ethereum/go-ethereum), separated out so nobody has to review a whole etherium project for the import. Can't be mofidied within non-LGPL-compatible projects for LGPL reasons, thanks Stallman. When generics release to the public I'll rewrite this to use those instead of interface{} and typechecks.